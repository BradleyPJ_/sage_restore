<?php

namespace Roots\Sage\Customizer;

use Roots\Sage\Assets;

/**
 * Add postMessage support
 */
function customize_register($wp_customize) {
  $wp_customize->get_setting('blogname')->transport = 'postMessage';

  //color
  $panel[] =array('slug' => 'colors', 'title' => 'Colors', 'description' => 'Options for all Colors');

  $section[] = array('slug'=>'Navigation_Colors','title' => 'Navigation Colors' ,'description' => 'Edit the Navigation Colors.','priority' => 55, 'panel' => 'colors');
  $section[] = array('slug'=>'General','title' => 'General Colors' ,'description' => 'Edit the General Colors.','priority' => 55, 'panel' => 'colors');
  $section[] = array('slug'=>'banner','title' => 'banner Colors' ,'description' => 'Edit the Banner Colors.','priority' => 55, 'panel' => 'colors');

  $standard_control[] = array('slug'=>'Navigation_Background_Color','default' => '#f3f3f3','label' => 'Pick Navigation Background Color ','type' => 'text', 'section' => 'Navigation_Colors', 'controller_type' => 'color');
  $standard_control[] = array('slug'=>'navigation_text_color','default' => '#222','label' => 'Pick Navigation Text Color ','type' => 'text', 'section' => 'Navigation_Colors', 'controller_type' => 'color');

  $standard_control[] = array('slug'=>'body_color','default' => '#fff','label' => 'Pick Body Color ','type' => 'text', 'section' => 'General', 'controller_type' => 'color');

  $standard_control[] = array('slug'=>'banner_background_color','default' => '#fff','label' => 'Pick Banner Background Color ','type' => 'text', 'section' => 'banner', 'controller_type' => 'color');


  //Banner
  $section[] = array('slug'=>'site_logo','title' => 'Site Logo' ,'description' => 'Select you site logo.','priority' => 55);

  $standard_control[] = array('slug'=>'site_logo','default' => NULL,'label' => 'Enter Logo ','type' => 'text', 'section' => 'site_logo', 'controller_type' => 'upload');

  //front_page
  $panel[] =array('slug' => 'front_page', 'title' => 'Front Page', 'description' => 'Options for the front page');

  $section[] = array('slug'=>'about','title' => 'About Options' ,'description' => 'Edit the About Options.','priority' => 55, 'panel' => 'front_page');
  $section[] = array('slug'=>'before_after','title' => 'before and after Options' ,'description' => 'Edit the before and after Options.','priority' => 55, 'panel' => 'front_page');

  $standard_control[] = array('slug'=>'about_title','default' => 'About','label' => 'Enter About Title','type' => 'Text', 'section' => 'about');
  $standard_control[] = array('slug'=>'about_content','default' => 'Main Text Content','label' => 'Enter About Content','type' => 'textarea', 'section' => 'about');

  $standard_control[] = array('slug'=>'image_before','default' => NULL,'label' => 'Enter Before Picture ','type' => 'text', 'section' => 'before_after', 'controller_type' => 'upload');
  $standard_control[] = array('slug'=>'image_before_content','default' => 'Caption Text','label' => 'Enter Caption Text','type' => 'textarea', 'section' => 'before_after');

  $standard_control[] = array('slug'=>'image_after','default' => NULL,'label' => 'Enter After Picture ','type' => 'text', 'section' => 'before_after', 'controller_type' => 'upload');
  $standard_control[] = array('slug'=>'image_after_content','default' => 'Caption Text','label' => 'Enter Caption Text','type' => 'textarea', 'section' => 'before_after');

  $standard_control[] = array('slug'=>'before_after_caption_background','default' => '#d6d6d6','label' => 'Pick before and after caption background Color ','type' => 'text', 'section' => 'before_after', 'controller_type' => 'color');
  $standard_control[] = array('slug'=>'before_after_caption_text','default' => '#222','label' => 'Pick before and after caption text Color ','type' => 'text', 'section' => 'before_after', 'controller_type' => 'color');


  foreach( $panel as $panel ) {

  $wp_customize->add_panel( $panel['slug'], array(
  'priority'       => 55,
  'capability'     => 'edit_theme_options',
  'theme_supports' => '',
  'title'          => __($panel['title']),
  'description'    => __($panel['description']),
  ) );
}

foreach( $section as $section ) {

  $wp_customize->add_section(
    $section['slug'],
      array(
      'title' => $section['title'],
      'description' => $section['description'],
      'priority' => $section['priority'],
      'panel'  => $section['panel'],
    )
  );
}

foreach( $standard_control as $standard_control ) {

  $wp_customize->add_setting($standard_control['slug'],
    array('default' => $standard_control['default'],'type' => 'option','capability' =>'edit_theme_options'
    )
  );

  if( $standard_control['controller_type'] === "upload"){

    $wp_customize->add_control(new \WP_Customize_Upload_Control($wp_customize, $standard_control['slug'],
      ['label'=> __( $standard_control['label'], 'sage' ),'section' => $standard_control['section'],'description' => $standard_control['description']] )
    );

  }elseif($standard_control['controller_type'] === "hidden"){

    $wp_customize->add_control(
        new \WP_Customize_Control($wp_customize,$standard_control['slug'],
          array('label' => $standard_control['label'],'settings' => $standard_control['slug'],'type' => $standard_control['type'])
        )
    );

  }elseif($standard_control['controller_type']=== "color"){

    $wp_customize->add_control(
        new \WP_Customize_Color_Control(
            $wp_customize,
            $standard_control['slug'],
            array(
                'label'      => $standard_control['label'],
                'section'    => $standard_control['section'],
                'settings'   => $standard_control['slug']
            )
        )
    );

  }else{

    $wp_customize->add_control(
        new \WP_Customize_Control($wp_customize,$standard_control['slug'],
          array('label' => $standard_control['label'],'section' => $standard_control['section'],'settings' => $standard_control['slug'],'type' => $standard_control['type'])
        )
    );

  }
}


}
add_action('customize_register', __NAMESPACE__ . '\\customize_register');

/**
 * Customizer JS
 */
function customize_preview_js() {
  wp_enqueue_script('sage/customizer', Assets\asset_path('scripts/customizer.js'), ['customize-preview'], null, true);
}
add_action('customize_preview_init', __NAMESPACE__ . '\\customize_preview_js');
