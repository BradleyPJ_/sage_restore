<section class="about">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3><?php echo get_option('about_title'); ?></h3>
        <p><?php echo get_option('about_content'); ?></p>
      </div>
    </div>
  </div>
</section>


<section class="before_after">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="col-md-6 before_after_images" >
          <img class = "img-responsive center-block" src="<?php echo get_option('image_before'); ?>"  alt="">
          <div class="caption">
            <?php echo get_option('image_before_content'); ?>
          </div>
        </div>
        <div class="col-md-6 before_after_images" >
          <img class = "img-responsive center-block" src="<?php echo get_option('image_after'); ?>"  alt="">
          <div class="caption">
            <?php echo get_option('image_after_content'); ?></div>
        </div>
      </div>
    </div>
  </div>
</section>
