<style>
body{background:<?php echo get_option('body_color'); ?>;}
.banner{background: <?php echo get_option('banner_background_color'); ?>;}
.navbar {background: <?php echo get_option('Navigation_Background_Color'); ?>;}
.nav>li>a {color:<?php echo get_option('navigation_text_color'); ?>;}
.before_after_images>.caption{
   background:<?php echo get_option('before_after_caption_background'); ?>;
   color:<?php echo get_option('before_after_caption_text'); ?>;
}
</style>
<section class="banner">
  <div class="container">
    <a class="" href="<?= esc_url(home_url('/')); ?>">
     <img alt="Brand" class = "img-responsive" src="<?php echo get_option('site_logo'); ?>"></a>
  </div>
</section>

<nav class="navbar ">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <?php
        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
        endif;
        ?>
      </ul>
    </div>
  </div>
</nav>
